$(document).ready(function () {
    $(".delete_doc").click(function () {
        var doctor_id = $(this).data('id')
        if (confirm("Are you sure you want to delete this Doctor?")) {
            $.ajax({
                type: "DELETE",
                url: "/doctor-delete",
                data: JSON.stringify({'data': doctor_id}),
                contentType: "application/json",
                dataType: 'json',
                success: function (response) {
                    if (response.status === 'success') {
                        $('#' + doctor_id).remove()
                        alert("Doctor Deleted Successfully")
                    }
                }
            })
        }
    })
})