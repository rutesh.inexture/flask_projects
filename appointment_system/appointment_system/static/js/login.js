$(document).ready(function () {
    $("#loader").hide()
    $("#login_form").validate({
        rules: {
            username: {
                required: true
            },
            password: {
                required: true,
            }
        },
        messages: {
            username: {
                required: "Please enter Username"
            },
            password: {
                required: "Please enter Password",
            }
        }
    });
})