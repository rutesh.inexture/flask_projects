$(document).ready(function () {
    $.validator.addMethod("character_only", function (value, element, param) {
        return value.match(new RegExp(".[a-zA-Z]$"));
    });
    $("#add_diseases_form").validate({
        rules: {
            disease_name: {
                required: true,
                character_only: true,
            }
        },
        messages: {
            disease_name: {
                required: "Please enter Disease Name",
                character_only: "Please enter Proper Diseases Name",
            }
        }
    });
    $("#submit").click(function (e) {
        e.preventDefault()
        if ($("#add_diseases_form").valid()) {
            var diseases_id = $("#diseases_id").val()
            var data = {};
            $.each($('#add_diseases_form').serializeArray(), function (_, kv) {
                data[kv.name] = kv.value;
            });
            $.ajax({
                type: "POST",
                url: "/diseases",
                data: JSON.stringify(data),
                contentType: "application/json",
                dataType: 'json',
                success: function (response) {
                    if(response.status == 'success')
                    {
                        document.location.href = '/diseases-list/'
                    }
                    else
                    {
                        if(response.type == 'Add'){
                            document.location.href = '/diseases-add'
                        }
                        else
                        {
                            document.location.href = '/diseases-update/'+diseases_id
                        }
                    }
                }
            })
        }
    })

})