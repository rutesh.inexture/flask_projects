$(document).ready(function () {
    $("#add_doctor_form").validate({
        rules: {
            name: {
                required: true
            },
            username: {
                required: true
            },
            phone: {
                required: true
            },
            email: {
                required: true,
                email: true
            },
            diseases_list:{
                required:true
            },
            address_1: {
                required: true
            },
            address_2: {
                required: true
            },
            city: {
                required: true,
            },
            state: {
                required: true,
            },
            pincode: {
                required: true,
            },
            country: {
                required: true,
            }
        },
        messages: {
            name: {
                required: "Please enter Name"
            },
            username: {
                required: "Please enter Username"
            },
            phone: {
                required: "Please enter Phone Number"
            },
            email: {
                required: "Please enter E-mail",
            },
            diseases_list:{
                required: "Please select Diseases",
            },
            address_1: {
                required: "Please enter Address Line 1"
            },
            address_2: {
                required: "Please enter Address Line 2"
            },
            city: {
                required: "Please enter City",
            },
            state: {
                required: "Please enter State",
            },
            pincode: {
                required: "Please enter Pincode",
            },
            country: {
                required: "Please enter Country",
            }
        }
    });
    $("#submit").click(function (e) {
        e.preventDefault()
        if ($("#add_doctor_form").valid()) {
            var doctors_id = $("#doctors_id").val()
            var data = {};
            $.each($('#add_doctor_form').serializeArray(), function (_, kv) {
                data[kv.name] = kv.value;
            });
            $.ajax({
                type: "POST",
                url: "/doctors",
                data: JSON.stringify(data),
                contentType: "application/json",
                dataType: 'json',
                success: function (response) {
                    if(response.status === 'success')
                    {
                        document.location.href = response.url
                    }
                    else
                    {
                        document.location.href = response.url
                    }
                }
            })
        }
    })
})