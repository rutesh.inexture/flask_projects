$(document).ready(function () {
    $(".delete_dis").click(function () {
        var disease_id = $(this).data('id')
        if (confirm("Are you sure you want to delete this Disease?")) {
            $.ajax({
                type: "DELETE",
                url: "/diseases-delete",
                data: JSON.stringify({'data': disease_id}),
                contentType: "application/json",
                dataType: 'json',
                success: function (response) {
                    if (response.status == 'success') {
                        // $(".alert.alert-success").text('Diseases Deleted Successfully')
                        alert("Diseases Deleted Successfully")
                        $('#' + disease_id).remove()
                    }
                }
            })
        }
    })
})