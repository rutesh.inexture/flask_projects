$(document).ready(function () {
    $("#appointment_form").validate({
        rules: {
            diseases: {
                required: true
            },
            doctors: {
                required: true
            },
            datetime: {
                required: true
            },
            description: {
                required: true,
            },
        },
        messages: {
            diseases: {
                required: "Please select Diseases"
            },
            doctors: {
                required: "Please select Doctor"
            },
            datetime: {
                required: "Please select Datetime"
            },
            description: {
                required: "Please enter Description",
            },
        }
    });
    $("#diseases").change(function () {
        $('#doctors option:not(:first)').remove();
        $.ajax({
            type: "POST",
            url: "/doctors_list_id",
            data: JSON.stringify({'data': this.value}),
            contentType: "application/json",
            // dataType: 'json',
            success: function (response) {
                console.log(response.doctors_data)
                if (response.success) {
                    $("#show_doc_error").hide()
                    var html = ''
                    for (var i = 0; i < response.doctors_data.length; i++) {
                        html += '<option value="' + response.doctors_data[i].id + '">' + response.doctors_data[i].name + '</option>'
                    }
                    $("#doctors").append(html)
                } else {
                    $('#doctors option:not(:first)').remove();
                    $("#show_doc_error").show()
                }
            }
        })
    })
    $("#submit").click(function (e) {
        e.preventDefault()
        if ($("#appointment_form").valid()) {
            var appointment_id = $("#appointment_id").val()
            var data = {};
            $.each($('#appointment_form').serializeArray(), function (_, kv) {
                data[kv.name] = kv.value;
            });
            $.ajax({
                type: "POST",
                url: "/appointments",
                data: JSON.stringify(data),
                contentType: "application/json",
                dataType: 'json',
                success: function (response) {
                    if (response.status === 'success') {
                        document.location.href = '/patient/appointments-list'
                    } else {
                        if (response.type === 'Add') {
                            document.location.href = '/patient/appointments-add'
                        } else {
                            document.location.href = '/patient/appointments-update/' + appointment_id
                        }
                    }
                }
            })
        }
    })
})