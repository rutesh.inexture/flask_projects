$(document).ready(function () {
    $("#next_btn").click(function (e) {
        if ($("#registration_form").valid()) {
            var password = $("#password").val()
            var confirm_password = $("#confirm_password").val()
            if (password == confirm_password) {
                $("#address_details").show()
                $("#user_details").hide()
            } else {
                $("#confirm_password-error").text('Password and Confirm Password must be same')
                $("#confirm_password-error").show()
            }
        }
    })
    $("#previous").click(function () {
        $("#user_details").show()
        $("#address_details").hide()
    })

    $("#registration_form").validate({
        rules: {
            name: {
                required: true
            },
            username: {
                required: true
            },
            phone: {
                required: true
            },
            email: {
                required: true,
                email: true
            },
            password: {
                required: true,
            },
            confirm_password: {
                required: true,
            },
        },
        messages: {
            name: {
                required: "Please enter Name"
            },
            username: {
                required: "Please enter Username"
            },
            phone: {
                required: "Please enter Phone Number"
            },
            email: {
                required: "Please enter E-mail",
            },
            password: {
                required: "Please enter Password",
            },
            confirm_password: {
                required: "Please enter Confirm Password",
            },
        }
    });
    $("#address_form").validate({
        rules: {
            address_1: {
                required: true
            },
            address_2: {
                required: true
            },
            city: {
                required: true,
            },
            state: {
                required: true,
            },
            pincode: {
                required: true,
            },
            country: {
                required: true,
            }
        },
        messages: {
            address_1: {
                required: "Please enter Address Line 1"
            },
            address_2: {
                required: "Please enter Address Line 2"
            },
            city: {
                required: "Please enter City",
            },
            state: {
                required: "Please enter State",
            },
            pincode: {
                required: "Please enter Pincode",
            },
            country: {
                required: "Please enter Country",
            }
        }
    });
    $("#submit").click(function () {
        if ($("#address_form").valid()) {
            var user_data = {
                'name': $("#name").val(), 'username': $("#username").val(), 'phone': $("#phone").val(),
                'email': $("#email").val(), 'password': $("#password").val()
            }
            $("#user_data").val(JSON.stringify(user_data))
            $("#address_form").submit()
        }
    })
})