$(document).ready(function () {
    $(".delete_appointment").click(function () {
        var appointment_id = $(this).data('id')
        if (confirm("Are you sure you want to delete this Appointment?")) {
            $.ajax({
                type: "DELETE",
                url: "/appointments-delete",
                data: JSON.stringify({'data': appointment_id}),
                contentType: "application/json",
                dataType: 'json',
                success: function (response) {
                    if (response.status == 'success') {
                        $(".alert.alert-success").text('Appointment Deleted Successfully')
                        alert("Appointment Deleted Successfully")
                        $('#' + appointment_id).remove()
                    }
                }
            })
        }
    })
})