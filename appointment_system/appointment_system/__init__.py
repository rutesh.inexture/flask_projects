from flask import Flask
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy
from flask_bcrypt import Bcrypt
from flask_login import LoginManager
from flask_mail import Mail
from appointment_system.config import Config

db = SQLAlchemy()
bcrypt = Bcrypt()
login_manager = LoginManager()
login_manager.login_view = 'users.login'
login_manager.login_message_category = 'info'

mail = Mail()


def create_app(config_class=Config):
    app = Flask(__name__, template_folder='./template')
    app.config.from_object(Config)
    migrate = Migrate(app, db)
    # print('---------------', bcrypt.generate_password_hash('doc_1@123').decode('utf-8'))
    from appointment_system import models

    db.init_app(app)
    with app.app_context():
        db.create_all()
    bcrypt.init_app(app)
    login_manager.init_app(app)
    mail.init_app(app)

    from appointment_system.users.routes import users
    from appointment_system.main.routes import main
    from appointment_system.doctor.routes import doctor
    from appointment_system.diseases.routes import diseases
    from appointment_system.patient.routes import patient
    from appointment_system.errors.handlers import errors

    app.register_blueprint(users)
    app.register_blueprint(main)
    app.register_blueprint(doctor)
    app.register_blueprint(diseases)
    app.register_blueprint(patient)
    app.register_blueprint(errors)

    return app
