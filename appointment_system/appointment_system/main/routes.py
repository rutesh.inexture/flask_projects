from flask import Blueprint, render_template, redirect, url_for, request
from flask.views import MethodView
from flask_login import current_user

from appointment_system.models import User, Diseases, Appointments, PatientAppointments

main = Blueprint('main', __name__)


class HomeView(MethodView):
    def get(self):
        if current_user.is_authenticated:
            if current_user.user_type.value == 'hospital_admin':
                return redirect(url_for('main.dashboard'))
            elif current_user.user_type.value == 'doctor':
                return redirect(url_for('main.doctor_dashboard'))
            elif current_user.user_type.value == 'patient':
                return redirect(url_for('main.patient_dashboard'))
        return render_template("home.html")


class Dashboard(MethodView):
    def get(self):
        if 'hospital/dashboard' in request.url_rule.rule:
            doctor_count = User.query.filter(User.user_type == 'doctor').count()
            diseases_count = Diseases.query.count()
            return render_template("hospital_dashboard.html", title="Hospital's Dashboard",
                                   counter={'doctor_count': doctor_count, 'diseases_count': diseases_count})
        elif 'doctor/dashboard' in request.url_rule.rule:
            appointment_counter = Appointments.query.filter(Appointments.user_id == current_user.id).count()
            return render_template("doctor_dashboard.html", title="Doctor's Dashboard",
                                   appointment_counter=appointment_counter)
        elif 'patient/dashboard' in request.url_rule.rule:
            appointment_counter = PatientAppointments.query \
                .join(Appointments, PatientAppointments.appointment_id == Appointments.id) \
                .filter(PatientAppointments.user_id == current_user.id).count()
            return render_template("patient_dashboard.html", title="Patient's Dashboard",
                                   appointment_counter=appointment_counter)


class AboutView(MethodView):
    def get(self):
        return render_template('about.html', title="About Page")


main.add_url_rule('/', view_func=HomeView.as_view('/'))
main.add_url_rule('/home', view_func=HomeView.as_view('home'))
main.add_url_rule('/about', view_func=AboutView.as_view('about'))
main.add_url_rule('/hospital/dashboard', view_func=Dashboard.as_view('dashboard'))
main.add_url_rule('/doctor/dashboard', view_func=Dashboard.as_view('doctor_dashboard'))
main.add_url_rule('/patient/dashboard', view_func=Dashboard.as_view('patient_dashboard'))

# @main.route('/')
# @main.route('/home')
# def home():
#     if current_user.is_authenticated:
#         if current_user.user_type.value == 'hospital_admin':
#             return redirect(url_for('main.dashboard'))
#         elif current_user.user_type.value == 'doctor':
#             return redirect(url_for('main.doctor_dashboard'))
#         elif current_user.user_type.value == 'patient':
#             return redirect(url_for('main.patient_dashboard'))
#     return render_template("home.html")


# @main.route('/hospital/dashboard')
# def dashboard():
#     doctor_count = User.query.filter(User.user_type == 'doctor').count()
#     diseases_count = Diseases.query.count()
#     return render_template("hospital_dashboard.html",
#                            counter={'doctor_count': doctor_count, 'diseases_count': diseases_count})


# @main.route('/doctor/dashboard')
# def doctor_dashboard():
#     appointment_counter = Appointments.query.filter(Appointments.user_id == current_user.id).count()
#     return render_template("doctor_dashboard.html", title="Doctor's Dashboard", appointment_counter=appointment_counter)
#
#
# @main.route('/patient/dashboard')
# def patient_dashboard():
#     appointment_counter = PatientAppointments.query\
#         .join(Appointments, PatientAppointments.appointment_id == Appointments.id)\
#         .filter(PatientAppointments.user_id == current_user.id).count()
#     return render_template("patient_dashboard.html", title="Patient's Dashboard",
#                            appointment_counter=appointment_counter)

# @main.route('/about')
# def about():
#     return render_template('about.html', title="About Page")
