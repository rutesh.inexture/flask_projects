from flask import Blueprint, render_template, redirect, url_for, request, flash, abort
from flask.views import MethodView
from flask_login import current_user, login_required
from sqlalchemy.orm import session

from appointment_system.models import User, Address, User_Address, Diseases, DoctorDisease, Appointments, \
    PatientAppointments
from appointment_system import db, bcrypt

doctor = Blueprint('doctor', __name__)


def get_address_id(address_id):
    address_data = Address.query.filter(id=address_id)
    return address_data


def get_all_diseases():
    diseases_list = Diseases.query.all()
    return diseases_list


@doctor.route('/doctor/dashboard')
@login_required
def dashboard():
    return render_template("home.html")


# @doctor.route('/doctor-list')
# @login_required
# def doctor_list():
#     doctor_data = User.query \
#         .join(User_Address, User.id == User_Address.user_id) \
#         .join(Address, Address.id == User_Address.address_id) \
#         .join(DoctorDisease, DoctorDisease.user_id == User.id) \
#         .join(Diseases, Diseases.id == DoctorDisease.diseases_id) \
#         .add_columns(User.id, User.name, User.phone, User.email, Address.address_1, Address.address_2,
#                      Address.city, Address.state, Address.pincode, Address.country, Diseases.name) \
#         .filter(User.user_type == 'doctor').all()
#     return render_template("doctor_list.html", title='Doctors List', doctor_data=doctor_data)


def get_doctor_data_id(doctor_id):
    try:
        doctor_data = User.query \
            .join(User_Address, User.id == User_Address.user_id) \
            .join(Address, Address.id == User_Address.address_id) \
            .join(DoctorDisease, DoctorDisease.user_id == doctor_id) \
            .add_columns(User.id, User.name, User.username, User.phone, User.email, Address.address_1,
                         Address.address_2,
                         Address.city, Address.state, Address.pincode, Address.country, DoctorDisease.diseases_id) \
            .filter(User.user_type == 'doctor') \
            .filter(User.id == doctor_id).all()
        return doctor_data[0]
    except (IndexError, AttributeError):
        abort(404)


# @doctor.route('/doctor-add', methods=['GET', 'POST'])
# @login_required
# def doctor_add():
#     disease_data = get_all_diseases()
#     if not disease_data:
#         flash(f'Please Insert Diseases First !', 'danger')
#         return redirect(url_for('diseases.diseases_list'))
#     if request.form:
#         address_1 = request.form['address_1']
#         address_2 = request.form['address_2']
#         city = request.form['city']
#         state = request.form['state']
#         pincode = request.form['pincode']
#         country = request.form['country']
#
#         address_exist = Address.query.filter(Address.address_1 == address_1,
#                                              Address.address_2 == address_2,
#                                              Address.city == city).first()
#         if address_exist:
#             address_id = address_exist.id
#         else:
#             address = Address(address_1=address_1, address_2=address_2, city=city, state=state, pincode=pincode,
#                               country=country)
#             db.session.add(address)
#             db.session.flush()
#             address_id = address.id
#
#         username_exist = User.query.filter(User.username == request.form['username']).first()
#         if username_exist:
#             flash(f'Username already exists !', 'danger')
#         else:
#             user = User(name=request.form['name'],
#                         phone=request.form['phone'], email=request.form['email'], username=request.form['username'],
#                         password=bcrypt.generate_password_hash(request.form['username'] + '@1234').decode('utf-8'),
#                         user_type='doctor')
#             db.session.add(user)
#             db.session.flush()
#             user_id = user.id
#
#             user_address = User_Address(user_id=user_id, address_id=address_id)
#             db.session.add(user_address)
#
#             doctor_disease = DoctorDisease(user_id=user_id, diseases_id=request.form['diseases_list'])
#             db.session.add(doctor_disease)
#
#             db.session.commit()
#             flash(f'Doctor Added Successfully !', 'success')
#             return redirect(url_for('doctor.doctor_list'))
#     return render_template('add_doctor.html', doctor_data=None, title='Add Doctors', disease_data=disease_data)
#
#
# @login_required
# @doctor.route('/doctor-update/<int:doctor_id>', methods=['GET', 'POST'])
# @doctor.route('/doctor/profile/<int:doctor_id>', methods=['GET', 'POST'])
# def doctor_update(doctor_id):
#     doctor_data = get_doctor_data_id(doctor_id)
#     disease_data = get_all_diseases()
#     if request.form:
#         address_1 = request.form['address_1']
#         address_2 = request.form['address_2']
#         city = request.form['city']
#         state = request.form['state']
#         pincode = request.form['pincode']
#         country = request.form['country']
#
#         address_exist = Address.query.filter(Address.address_1 == address_1,
#                                              Address.address_2 == address_2,
#                                              Address.city == city).first()
#         if address_exist:
#             address_id = address_exist.id
#             address = Address.query.get_or_404(address_id)
#             address.address_1 = address_1
#             address.address_2 = address_2
#             address.city = city
#             address.state = state
#             address.pincode = pincode
#             address.country = country
#         else:
#             address = Address(address_1=address_1, address_2=address_2, city=city, state=state, pincode=pincode,
#                               country=country)
#             db.session.add(address)
#             db.session.flush()
#             address_id = address.id
#
#         username_exist = User.query.filter(User.username == request.form['username']).filter(
#             User.id != doctor_id).first()
#         if username_exist:
#             flash(f'Username already exists !', 'danger')
#             return redirect(url_for('doctor.doctor_update', doctor_id=doctor_id))
#         else:
#             user_data = User.query.get_or_404(doctor_id)
#             user_data.name = request.form['name']
#             user_data.username = request.form['username']
#             user_data.phone = request.form['phone']
#             user_data.email = request.form['email']
#
#             if not address_exist:
#                 user_address_data = User_Address.query.get_or_404(user_id=doctor_id)
#                 if user_address_data:
#                     user_address_data.user_id = doctor_id
#                     user_address_data.address_id = address_id
#                 else:
#                     user_address = User_Address(user_id=doctor_id, address_id=address_id)
#                     db.session.add(user_address)
#             db.session.commit()
#             if 'doctor/profile' in request.url_rule.rule:
#                 flash(f'Profile Updated Successfully !', 'success')
#                 return redirect(url_for('doctor.doctor_update', doctor_id=doctor_id))
#             else:
#                 flash(f'Doctor Updated Successfully !', 'success')
#                 return redirect(url_for('doctor.doctor_list'))
#     return render_template('add_doctor.html', doctor_data=doctor_data, title='Profile Update',
#                            disease_data=disease_data)


# @login_required
# @doctor.route('/doctor-delete/<int:doctor_id>', methods=['GET', 'POST'])
# def doctor_delete(doctor_id):
#     user = User.query.get_or_404(doctor_id)
#     db.session.delete(user)
#     db.session.commit()
#     flash(f'Doctor Deleted Successfully', 'success')
#     return redirect(url_for('doctor.doctor_list'))


@login_required
@doctor.route('/doctor-appointments/', methods=['GET', 'POST'])
def doctor_appointments():
    appointments_data = Appointments.query \
        .join(PatientAppointments, Appointments.id == PatientAppointments.appointment_id) \
        .join(User, User.id == PatientAppointments.user_id) \
        .join(Diseases, Diseases.id == Appointments.diseases_id) \
        .filter(Appointments.user_id == current_user.id) \
        .add_columns(User.name, Diseases.name, Appointments.apt_datetime, Appointments.is_approved, Appointments.id) \
        .all()
    return render_template('appointment_list.html', title="Doctor's Appointment List",
                           appointments_data=appointments_data, hide_add_btn=False)


@login_required
@doctor.route('/appointment-status/', methods=['GET', 'POST'])
def update_appointment_status():
    type = request.args.get('type')
    appointment_id = request.args.get('appointment_id')
    appointments_data = Appointments.query.get_or_404(appointment_id)
    if type == '1':
        appointments_data.is_approved = 1
        db.session.commit()
        flash(f'Appointment Approved Successfully !', 'success')
    elif type == '2':
        appointments_data.is_approved = 2
        db.session.commit()
        flash(f'Appointment Rejected Successfully !', 'danger')
    return redirect(url_for('doctor.doctor_appointments'))


class DoctorView(MethodView):
    def get(self, doctors_id=None):
        if '/doctor-list' in request.url_rule.rule:
            doctor_data = User.query \
                .join(User_Address, User.id == User_Address.user_id) \
                .join(Address, Address.id == User_Address.address_id) \
                .join(DoctorDisease, DoctorDisease.user_id == User.id) \
                .join(Diseases, Diseases.id == DoctorDisease.diseases_id) \
                .add_columns(User.id, User.name, User.phone, User.email, Address.address_1, Address.address_2,
                             Address.city, Address.state, Address.pincode, Address.country, Diseases.name) \
                .filter(User.user_type == 'doctor').all()
            return render_template("doctor_list.html", title='Doctors List', doctor_data=doctor_data)
        elif '/doctor-add' in request.url_rule.rule:
            disease_data = get_all_diseases()
            return render_template('add_doctor.html', doctor_data=None, title='Add Doctors', disease_data=disease_data)
        elif ('/doctor_update' in request.url_rule.rule) or ('/doctor-profile' in request.url_rule.rule):
            disease_data = get_all_diseases()
            doctor_data = get_doctor_data_id(doctors_id)
            return render_template('add_doctor.html', doctor_data=doctor_data, title='Profile Update',
                                   disease_data=disease_data)

    def post(self):
        form_data = request.json
        address_1 = form_data['address_1']
        address_2 = form_data['address_2']
        city = form_data['city']
        state = form_data['state']
        pincode = form_data['pincode']
        country = form_data['country']
        doctors_id = form_data['doctors_id']

        disease_data = get_all_diseases()
        if not disease_data:
            flash(f'Please Insert Diseases First !', 'danger')
            response = {'status': 'error', 'type': 'Add', 'url': 'diseases-list/'}
            return response

        if doctors_id == "":
            doc_name = form_data['name']
            doc_phone = form_data['phone']
            doc_email = form_data['email']
            doc_username = form_data['username']
            doc_password = bcrypt.generate_password_hash(form_data['username'] + '@1234').decode('utf-8')

            address_exist = Address.query.filter(Address.address_1 == address_1, Address.address_2 == address_2,
                                                 Address.city == city).first()
            if address_exist:
                address_id = address_exist.id
            else:
                address = Address(address_1=address_1, address_2=address_2, city=city, state=state, pincode=pincode,
                                  country=country)
                db.session.add(address)
                db.session.flush()
                address_id = address.id

            username_exist = User.query.filter(User.username == doc_username).first()
            if username_exist:
                flash(f'Username already exists !', 'danger')
                response = {'status': 'error', 'type': 'Add', 'url': '/doctor-add'}
                return response
            else:
                user = User(name=doc_name, phone=doc_phone, email=doc_email, username=doc_username,
                            password=doc_password, user_type='doctor')
                db.session.add(user)
                db.session.flush()
                user_id = user.id

                user_address = User_Address(user_id=user_id, address_id=address_id)
                db.session.add(user_address)

                doctor_disease = DoctorDisease(user_id=user_id, diseases_id=form_data['diseases_list'])
                db.session.add(doctor_disease)

                db.session.commit()
                flash(f'Doctor Added Successfully !', 'success')
                response = {'status': 'success', 'type': 'Add', 'url': '/doctor-list'}
                return response
        else:
            address_exist = Address.query.filter(Address.address_1 == address_1, Address.address_2 == address_2,
                                                 Address.city == city).first()
            if address_exist:
                address_id = address_exist.id
                address = Address.query.get_or_404(address_id)
                address.address_1 = address_1
                address.address_2 = address_2
                address.city = city
                address.state = state
                address.pincode = pincode
                address.country = country
            else:
                address = Address(address_1=address_1, address_2=address_2, city=city, state=state, pincode=pincode,
                                  country=country)
                db.session.add(address)
                db.session.flush()
                address_id = address.id

            username_exist = User.query.filter(User.username == form_data['username']).filter(
                User.id != doctors_id).first()
            if username_exist:
                flash(f'Username already exists !', 'danger')
                doc_url = '/doctor_update/' + doctors_id
                response = {'status': 'error', 'type': 'Update', 'url': doc_url}
                return response
            else:
                user_data = User.query.get_or_404(doctors_id)
                user_data.name = form_data['name']
                user_data.username = form_data['username']
                user_data.phone = form_data['phone']
                user_data.email = form_data['email']

                if not address_exist:
                    user_address_data = User_Address.query.get_or_404(user_id=doctors_id)
                    if user_address_data:
                        user_address_data.user_id = doctors_id
                        user_address_data.address_id = address_id
                    else:
                        user_address = User_Address(user_id=doctors_id, address_id=address_id)
                        db.session.add(user_address)
                db.session.commit()
                if form_data['doc_type'] == 'profile':
                    flash(f'Profile Updated Successfully !', 'success')
                    doc_url = '/doctor-profile/' + doctors_id
                    response = {'status': 'success', 'type': 'Update', 'url': doc_url}
                    return response
                elif form_data['doc_type'] == 'update':
                    flash(f'Doctor Updated Successfully !', 'success')
                    response = {'status': 'success', 'type': 'Update', 'url': '/doctor-list'}
                    return response

    def delete(self):
        user = User.query.get_or_404(request.json['data'])
        db.session.delete(user)
        db.session.commit()
        flash(f'Doctor Deleted Successfully', 'success')
        response = {'status': 'success'}
        return response


doctor.add_url_rule('/doctor-list', view_func=DoctorView.as_view('doctor_list'), methods=['GET'])

doctor.add_url_rule('/doctor-add', view_func=DoctorView.as_view('doctor_add'), methods=['GET'])
doctor.add_url_rule('/doctor_update/<int:doctors_id>', view_func=DoctorView.as_view('doctor_update'),
                    methods=['GET'])
doctor.add_url_rule('/doctor-profile/<int:doctors_id>', view_func=DoctorView.as_view('profile'),
                    methods=['GET'])
doctor.add_url_rule('/doctors', view_func=DoctorView.as_view('doctors'), methods=['POST'])
doctor.add_url_rule('/doctor-delete', view_func=DoctorView.as_view('doctor-delete'), methods=['DELETE'])
