import json

from flask import Blueprint, render_template, redirect, url_for, request, flash
from flask.views import MethodView, View

from appointment_system import db
from appointment_system.models import Diseases

diseases = Blueprint('diseases', __name__)


class DiseasesView(MethodView):
    def get(self, disease_id=None):
        if '/diseases-list' in request.url_rule.rule:
            disease_data = Diseases.query.all()
            return render_template("diseases_list.html", title="Diseases List", disease_data=disease_data)
        elif '/diseases-add' in request.url_rule.rule:
            return render_template("add_diseases.html", title="Add Diseases", disease_data=None)
        elif '/diseases-update' in request.url_rule.rule:
            disease_data = Diseases.query.get_or_404(disease_id)
            return render_template("add_diseases.html", title="Update Diseases", disease_data=disease_data)

    def post(self):
        form_data = request.json
        diseases_name = form_data['disease_name']
        disease_id = form_data['diseases_id']
        if disease_id == "":
            disease_exist = Diseases.query.filter(Diseases.name == diseases_name).first()
            if not disease_exist:
                diseases = Diseases(name=diseases_name)
                db.session.add(diseases)
                db.session.commit()
                flash(f'Disease Added Successfully !', 'success')
                response = {'status': 'success', 'type': 'Add'}
                return response
                # return redirect(url_for('diseases.diseases_list'))

            else:
                flash(f'Disease already exists !', 'danger')
                response = {'status': 'error', 'type': 'Add'}
                return response
                # return redirect(url_for('diseases.diseases_add'))
        else:
            disease_exist = Diseases.query.filter(Diseases.name == diseases_name).filter(
                Diseases.id != disease_id).first()
            if disease_exist:
                flash(f'Disease already exists !', 'danger')
                response = {'status': 'error', 'type': 'Update'}
                return response
                # return redirect(url_for('diseases.diseases_update', disease_id=disease_id))
            else:
                disease_data = Diseases.query.get_or_404(disease_id)
                disease_data.name = diseases_name
                db.session.commit()
                flash(f'Disease Updated Successfully !', 'success')
                response = {'status': 'success', 'type': 'Update'}
                return response
                # return redirect(url_for('diseases.diseases_list'))

    def delete(self):
        diseases = Diseases.query.get_or_404(request.json['data'])
        db.session.delete(diseases)
        db.session.commit()
        flash(f'Diseases Deleted Successfully', 'success')
        response = {'status': 'success'}
        return response
        # return redirect(url_for('diseases.diseases_list'))


diseases.add_url_rule('/diseases-list/', view_func=DiseasesView.as_view('diseases_list'), methods=['GET'])
diseases.add_url_rule('/diseases-add', view_func=DiseasesView.as_view('diseases_add'), methods=['GET' ])
diseases.add_url_rule('/diseases-update/<int:disease_id>', view_func=DiseasesView.as_view('diseases_update'),
                      methods=['GET'])
diseases.add_url_rule('/diseases', view_func=DiseasesView.as_view('diseases'), methods=['POST'])
diseases.add_url_rule('/diseases-delete', view_func=DiseasesView.as_view('diseases_delete'),
                      methods=['DELETE'])

# class DiseasesView(View):
#     methods = ['GET', 'POST']
#
#     def dispatch_request(self, disease_id=None):
#         if request.method == 'GET':
#             if '/diseases-list' in request.url_rule.rule:
#                 disease_data = Diseases.query.all()
#                 return render_template("diseases_list.html", title="Diseases List", disease_data=disease_data)
#             elif '/diseases-add' in request.url_rule.rule:
#                 return render_template("add_diseases.html", title="Add Diseases", disease_data=None)
#             elif '/diseases-update' in request.url_rule.rule:
#                 disease_data = Diseases.query.get_or_404(disease_id)
#                 return render_template("add_diseases.html", title="Update Diseases", disease_data=disease_data)
#             elif '/diseases-delete' in request.url_rule.rule:
#                 diseases = Diseases.query.get_or_404(disease_id)
#                 db.session.delete(diseases)
#                 db.session.commit()
#                 flash(f'Diseases Deleted Successfully', 'success')
#                 return redirect(url_for('diseases.diseases_list'))
#         elif request.method == 'POST':
#             disease_name = request.form['disease_name']
#             if request.form['diseases_id'] == "":
#                 disease_exist = Diseases.query.filter(Diseases.name == disease_name).first()
#                 if not disease_exist:
#                     diseases = Diseases(name=disease_name)
#                     db.session.add(diseases)
#                     db.session.commit()
#                     flash(f'Disease Added Successfully !', 'success')
#                     return redirect(url_for('diseases.diseases_list'))
#                 else:
#                     flash(f'Disease already exists !', 'danger')
#                     return redirect(url_for('diseases.diseases_add'))
#             else:
#                 disease_exist = Diseases.query.filter(Diseases.name == disease_name).filter(
#                     Diseases.id != disease_id).first()
#                 if disease_exist:
#                     flash(f'Disease already exists !', 'danger')
#                     return redirect(url_for('diseases.diseases_update', disease_id=disease_id))
#                 else:
#                     disease_data = Diseases.query.get_or_404(disease_id)
#                     disease_data.name = disease_name
#                     db.session.commit()
#                 flash(f'Disease Updated Successfully !', 'success')
#                 return redirect(url_for('diseases.diseases_list'))
#
#
# diseases.add_url_rule('/diseases-list/', view_func=DiseasesView.as_view('diseases_list'))
# diseases.add_url_rule('/diseases-add/', view_func=DiseasesView.as_view('diseases_add'))
# diseases.add_url_rule('/diseases-update/<int:disease_id>', view_func=DiseasesView.as_view('diseases_update'))
# diseases.add_url_rule('/diseases-delete/<int:disease_id>', view_func=DiseasesView.as_view('diseases_delete'))


# @login_required
# @diseases.route('/diseases-list')
# def diseases_list():
#     disease_data = Diseases.query.all()
#     return render_template("diseases_list.html", title="Diseases List", disease_data=disease_data)


# @login_required
# @diseases.route('/diseases-add', methods=['GET', 'POST'])
# def diseases_add():
#     if request.form:
#         diseases_name = request.form['disease_name']
#
#         disease_exist = Diseases.query.filter(Diseases.name == diseases_name).first()
#         if not disease_exist:
#             diseases = Diseases(name=diseases_name)
#             db.session.add(diseases)
#             db.session.commit()
#             flash(f'Disease Added Successfully !', 'success')
#             return redirect(url_for('diseases.diseases_list'))
#         else:
#             flash(f'Disease already exists !', 'danger')
#             return redirect(url_for('diseases.diseases_add'))
#     return render_template("add_diseases.html", title="Add Diseases", disease_data=None)
#
#
# @login_required
# @diseases.route('/diseases-update/<int:disease_id>', methods=['GET', 'POST'])
# def diseases_update(disease_id):
#     disease_data = Diseases.query.get_or_404(disease_id)
#     if request.form:
#         disease_name = request.form['disease_name']
#         disease_exist = Diseases.query.filter(Diseases.name == disease_name).filter(Diseases.id != disease_id).first()
#         if disease_exist:
#             flash(f'Disease already exists !', 'danger')
#             return redirect(url_for('diseases.diseases_update', disease_id=disease_id))
#         else:
#             disease_data = Diseases.query.get_or_404(disease_id)
#             disease_data.name = disease_name
#             db.session.commit()
#             flash(f'Disease Updated Successfully !', 'success')
#             return redirect(url_for('diseases.diseases_list'))
#     return render_template("add_diseases.html", title="Update Diseases", disease_data=disease_data)
#
#
# @diseases.route('/diseases-delete/<int:disease_id>', methods=['GET', 'POST'])
# @login_required
# def diseases_delete(disease_id):
#     diseases = Diseases.query.get_or_404(disease_id)
#     db.session.delete(diseases)
#     db.session.commit()
#     flash(f'Diseases Deleted Successfully', 'success')
#     return redirect(url_for('diseases.diseases_list'))
