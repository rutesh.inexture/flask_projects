import json

from flask import Blueprint, render_template, request, flash, redirect, url_for
from flask.views import MethodView
from flask_login import login_user, logout_user

from appointment_system import db, bcrypt
from appointment_system.models import User, Address, User_Address

users = Blueprint('users', __name__)


class UsersView(MethodView):
    template_name = 'register.html'

    def get(self):
        return render_template(self.template_name, title='Register', patient_data=None)

    def post(self):
        if request.form:
            data = json.loads(request.form['user_data'])

            address_1 = request.form['address_1']
            address_2 = request.form['address_2']
            city = request.form['city']
            state = request.form['state']
            pincode = request.form['pincode']
            country = request.form['country']

            address_exist = Address.query.filter(Address.address_1 == address_1,
                                                 Address.address_2 == address_2,
                                                 Address.city == city).first()
            if address_exist:
                address_id = address_exist.id
                address = Address.query.get_or_404(address_id)
                address.address_1 = address_1
                address.address_2 = address_2
                address.city = city
                address.state = state
                address.pincode = pincode
                address.country = country
            else:
                address = Address(address_1=address_1, address_2=address_2, city=city, state=state, pincode=pincode,
                                  country=country)
                db.session.add(address)
                db.session.flush()
                address_id = address.id

            username_exist = User.query.filter(User.username == data['username']).first()
            if username_exist:
                flash(f'Username already exists !', 'danger')
            else:
                user = User(name=data['name'],
                            phone=data['phone'], email=data['email'], username=data['username'],
                            password=bcrypt.generate_password_hash(data['password']).decode('utf-8'),
                            user_type='patient')
                db.session.add(user)
                db.session.flush()
                user_id = user.id

                user_address = User_Address(user_id=user_id, address_id=address_id)
                db.session.add(user_address)

                db.session.commit()
                flash(f'Patient Added Successfully !', 'success')
                return redirect(url_for('users.login', type='patient'))


class Login(MethodView):
    template_name = 'login.html'

    def get(self):
        user_type = request.args.get('type')
        return render_template(self.template_name, title='Login', type=user_type)

    def post(self):
        user_type = request.args.get('type')
        if request.form:
            user = User.query \
                .filter((User.username == request.form['username']) | (User.email == request.form['username'])) \
                .filter(User.user_type == user_type) \
                .first()
            if user and bcrypt.check_password_hash(user.password, request.form['password']):
                login_user(user)
                next_page = request.args.get('next')
                flash(f"Login Successfully, Welcome {user.name}!", 'success')
                if user_type == 'hospital_admin':
                    return redirect(next_page) if next_page else redirect(url_for('main.home'))
                elif user_type == 'doctor':
                    return redirect(next_page) if next_page else redirect(url_for('main.doctor_dashboard'))
                elif user_type == 'patient':
                    return redirect(next_page) if next_page else redirect(url_for('main.patient_dashboard'))
            else:
                flash(f"Invalid Email & Password", 'danger')
                return redirect(url_for('users.login', type=user_type))


class Logout(MethodView):
    def get(self):
        logout_user()
        return redirect(url_for('main.home'))


users.add_url_rule('/register', view_func=UsersView.as_view('register'))
users.add_url_rule('/login', view_func=Login.as_view('login'))
users.add_url_rule('/logout', view_func=Logout.as_view('logout'))

# @users.route('/register', methods=['GET', 'POST'])
# def register():
#     if request.form:
#         data = json.loads(request.form['user_data'])
#
#         address_1 = request.form['address_1']
#         address_2 = request.form['address_2']
#         city = request.form['city']
#         state = request.form['state']
#         pincode = request.form['pincode']
#         country = request.form['country']
#
#         address_exist = Address.query.filter(Address.address_1 == address_1,
#                                              Address.address_2 == address_2,
#                                              Address.city == city).first()
#         if address_exist:
#             address_id = address_exist.id
#             address = Address.query.get_or_404(address_id)
#             address.address_1 = address_1
#             address.address_2 = address_2
#             address.city = city
#             address.state = state
#             address.pincode = pincode
#             address.country = country
#         else:
#             address = Address(address_1=address_1, address_2=address_2, city=city, state=state, pincode=pincode,
#                               country=country)
#             db.session.add(address)
#             db.session.flush()
#             address_id = address.id
#
#         username_exist = User.query.filter(User.username == data['username']).first()
#         if username_exist:
#             flash(f'Username already exists !', 'danger')
#         else:
#             user = User(name=data['name'],
#                         phone=data['phone'], email=data['email'], username=data['username'],
#                         password=bcrypt.generate_password_hash(data['password']).decode('utf-8'),
#                         user_type='patient')
#             db.session.add(user)
#             db.session.flush()
#             user_id = user.id
#
#             user_address = User_Address(user_id=user_id, address_id=address_id)
#             db.session.add(user_address)
#
#             db.session.commit()
#             flash(f'Patient Added Successfully !', 'success')
#             return redirect(url_for('users.login', type='patient'))
#
#     return render_template('register.html', title='Register', patient_data=None)


# @users.route('/login', methods=['GET', 'POST'])
# def login():
#     user_type = request.args.get('type', '0', type=str)
#     if current_user.is_authenticated:
#         return redirect(url_for('main.home'))
#     if request.form:
#         user = User.query \
#             .filter((User.username == request.form['username']) | (User.email == request.form['username'])) \
#             .filter(User.user_type == user_type) \
#             .first()
#         if user and bcrypt.check_password_hash(user.password, request.form['password']):
#             login_user(user)
#             next_page = request.args.get('next')
#             flash(f"Login Successfully, Welcome {user.name}!", 'success')
#             if user_type == 'hospital_admin':
#                 return redirect(next_page) if next_page else redirect(url_for('main.home'))
#             elif user_type == 'doctor':
#                 return redirect(next_page) if next_page else redirect(url_for('main.doctor_dashboard'))
#             elif user_type == 'patient':
#                 return redirect(next_page) if next_page else redirect(url_for('main.patient_dashboard'))
#         else:
#             flash(f"Invalid Email & Password", 'danger')
#             return redirect(url_for('users.login', type=user_type))
#     return render_template('login.html', title='Login', type=user_type)


# @users.route('/logout')
# def logout():
#     logout_user()
#     return redirect(url_for('main.home'))

#
# @users.route('/reset_password', methods=['GET', 'POST'])
# def reset_request():
#     if current_user.is_authenticated:
#         return redirect(url_for('main.home'))
#     form = RequestResetForm()
#     if form.validate_on_submit():
#         user = User.query.filter_by(email=form.email.data).first()
#         send_reset_email(user)
#         flash("Email has been sent with instructions to reset password.", 'info')
#         return redirect(url_for('users.login'))
#     return render_template('reset_request.html', title='Reset Password', form=form)
#
#
# @users.route('/reset_password/<token>', methods=['GET', 'POST'])
# def reset_token(token):
#     if current_user.is_authenticated:
#         return redirect(url_for('main.home'))
#     user = User.verify_reset_token(token)
#     if not user:
#         flash("Invalid or Expired Token", "warning")
#         return redirect(url_for('users.reset_request'))
#     form = ResetPasswordForm()
#     if form.validate_on_submit():
#         hashed_password = bcrypt.generate_password_hash(form.password.data).decode('utf-8')
#         user.password = hashed_password
#         db.session.commit()
#         flash(f"Your Password has been Updated! You are now able to Log In", 'success')
#         return redirect(url_for('users.login'))
#     return render_template('reset_token.html', title='Reset Password', form=form)
