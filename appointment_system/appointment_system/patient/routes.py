import json
from datetime import datetime

from flask import Blueprint, render_template, redirect, url_for, request, flash, abort
from flask.views import MethodView
from flask_login import current_user

from appointment_system import db
from appointment_system.doctor.routes import get_all_diseases
from appointment_system.models import User, Address, User_Address, DoctorDisease, Appointments, PatientAppointments, \
    Diseases

patient = Blueprint('patient', __name__)


def get_patient_data_id(patient_id):
    patient_data = User.query \
        .join(User_Address, User.id == User_Address.user_id) \
        .join(Address, Address.id == User_Address.address_id) \
        .add_columns(User.id, User.name, User.username, User.phone, User.email, Address.address_1, Address.address_2,
                     Address.city, Address.state, Address.pincode, Address.country) \
        .filter(User.user_type == 'patient') \
        .filter(User.id == patient_id).all()
    return patient_data


def get_appointment_data_id(appointment_id):
    try:
        appointment_data = Appointments.query \
            .join(PatientAppointments, Appointments.id == PatientAppointments.appointment_id) \
            .join(User, Appointments.user_id == User.id) \
            .join(Diseases, Appointments.diseases_id == Diseases.id) \
            .filter(PatientAppointments.user_id == current_user.id) \
            .filter(Appointments.id == appointment_id) \
            .add_columns(User.name, Diseases.name, Appointments.apt_datetime, Appointments.is_approved, Appointments.id,
                         User.id) \
            .all()

        if appointment_data[0].is_approved:
            flash(f"You can't update this Appointment as it has Approved by Doctor !", 'warning')
        return appointment_data[0]
    except (IndexError, AttributeError):
        abort(404)


def get_doctor_diseases_id(diseases_id):
    doctors_data = DoctorDisease.query \
        .join(User, DoctorDisease.user_id == User.id) \
        .filter(DoctorDisease.diseases_id == diseases_id) \
        .add_columns(User.id, User.name) \
        .all()
    doctor_dict = []
    for index in range(len(doctors_data)):
        doctor_dict.append({'id': doctors_data[index].id, 'name': doctors_data[index].name})
    return doctor_dict


class PatientProfile(MethodView):
    template_name = 'register.html'

    def get(self, patient_id):
        patient_data = get_patient_data_id(patient_id)
        return render_template(self.template_name, title='Profile Update', patient_data=patient_data)

    def post(self, patient_id):
        if request.form:
            data = json.loads(request.form['user_data'])

            address_1 = request.form['address_1']
            address_2 = request.form['address_2']
            city = request.form['city']
            state = request.form['state']
            pincode = request.form['pincode']
            country = request.form['country']

            address_exist = Address.query.filter(Address.address_1 == address_1,
                                                 Address.address_2 == address_2,
                                                 Address.city == city).first()
            if address_exist:
                address_id = address_exist.id
                address = Address.query.get_or_404(address_id)
                address.address_1 = address_1
                address.address_2 = address_2
                address.city = city
                address.state = state
                address.pincode = pincode
                address.country = country
            else:
                address = Address(address_1=address_1, address_2=address_2, city=city, state=state, pincode=pincode,
                                  country=country)
                db.session.add(address)
                db.session.flush()
                address_id = address.id

            username_exist = User.query.filter(User.username == data['username']).filter(User.id != patient_id).first()
            if username_exist:
                flash(f'Username already exists !', 'danger')
            else:
                user_data = User.query.get_or_404(patient_id)
                user_data.name = data['name']
                user_data.username = data['username']
                user_data.phone = data['phone']
                user_data.email = data['email']

                if not address_exist:
                    user_address_data = User_Address.query.get_or_404(user_id=patient_id)
                    if user_address_data:
                        user_address_data.user_id = patient_id
                        user_address_data.address_id = address_id
                    else:
                        user_address = User_Address(user_id=patient_id, address_id=address_id)
                        db.session.add(user_address)

                db.session.commit()
                flash(f'Profile Updated Successfully !', 'success')
                return redirect(url_for('patient.patient_profile', patient_id=patient_id))


class AppointmentView(MethodView):
    def get(self, appointment_id=None):
        if '/patient/appointments-list' in request.url_rule.rule:
            appointments_data = Appointments.query \
                .join(PatientAppointments, Appointments.id == PatientAppointments.appointment_id) \
                .join(User, Appointments.user_id == User.id) \
                .join(Diseases, Appointments.diseases_id == Diseases.id) \
                .filter(PatientAppointments.user_id == current_user.id) \
                .add_columns(User.name, Diseases.name, Appointments.apt_datetime, Appointments.is_approved,
                             Appointments.id, Appointments.description) \
                .all()
            return render_template('appointment_list.html', title='Appointment List',
                                   appointments_data=appointments_data, hide_add_btn=True)
        elif '/patient/appointments-add' in request.url_rule.rule:
            all_diseases = get_all_diseases()
            return render_template('add_appointment.html', title='Add Appointment', diseases=all_diseases,
                                   appointment_data=None)
        elif '/patient/appointments-update' in request.url_rule.rule:
            all_diseases = get_all_diseases()
            appointment_data = get_appointment_data_id(appointment_id)
            doctor_data = get_doctor_diseases_id(appointment_data.Appointments.diseases_id)
            return render_template('add_appointment.html', title='Update Appointment', diseases=all_diseases,
                                   appointment_data=appointment_data, doctor_data=doctor_data)

    def post(self):
        form_data = request.json
        custom_datetime = datetime.strptime(form_data['datetime'], '%Y-%m-%dT%H:%M')
        doctors_id = form_data['doctors']
        diseases_id = form_data['diseases']
        description = form_data['description']
        appointment_id = form_data['appointment_id']
        if appointment_id == "":
            appointment_data = Appointments(user_id=doctors_id, diseases_id=diseases_id,
                                            apt_datetime=custom_datetime, description=description)
            db.session.add(appointment_data)
            db.session.flush()
            last_appointment_id = appointment_data.id

            patient_appointment = PatientAppointments(user_id=current_user.id, appointment_id=last_appointment_id)
            db.session.add(patient_appointment)
            db.session.commit()
            flash(f'Appointment Added Successfully !', 'success')
            response = {'status': 'success', 'type': 'Add'}
            return response
        else:
            appointment_details = Appointments.query.get_or_404(appointment_id)
            appointment_details.diseases_id = diseases_id
            appointment_details.user_id = doctors_id
            appointment_details.apt_datetime = custom_datetime
            appointment_details.description = description
            db.session.commit()
            flash(f'Appointment Updated Successfully !', 'success')
            response = {'status': 'success', 'type': 'Update'}
            return response

    # def put(self, appointment_id):
    #     if request.form:
    #         custom_datetime = datetime.strptime(request.form['datetime'], '%Y-%m-%dT%H:%M')
    #         appointment_details = Appointments.query.get_or_404(appointment_id)
    #         appointment_details.diseases_id = request.form['diseases']
    #         appointment_details.user_id = request.form['doctors']
    #         appointment_details.apt_datetime = custom_datetime
    #         appointment_details.description = request.form['description']
    #         db.session.commit()
    #         flash(f'Appointment Updated Successfully !', 'success')
    #         return redirect(url_for('patient.appointments_list'))

    def delete(self):
        appointment = Appointments.query.get_or_404(request.json['data'])
        db.session.delete(appointment)
        db.session.commit()
        flash(f'Appointment Deleted Successfully', 'success')
        response = {'status': 'success'}
        return response


class DoctorListDiseasesID(MethodView):
    def post(self):
        doctors_data = DoctorDisease.query \
            .join(User, DoctorDisease.user_id == User.id) \
            .filter(DoctorDisease.diseases_id == request.json['data']) \
            .add_columns(User.id, User.name) \
            .all()
        doctor_dict = []
        for index in range(len(doctors_data)):
            doctor_dict.append({'id': doctors_data[index].id, 'name': doctors_data[index].name})

        context = {
            'success': True if doctors_data else False,
            'doctors_data': doctor_dict
        }
        return context


patient.add_url_rule('/patient/profile/<int:patient_id>', view_func=PatientProfile.as_view('patient_profile'),
                     methods=['GET', 'POST'])

patient.add_url_rule('/patient/appointments-list', view_func=AppointmentView.as_view('appointments_list'),
                     methods=['GET'])
patient.add_url_rule('/patient/appointments-add', view_func=AppointmentView.as_view('appointments-add'),
                     methods=['GET'])
patient.add_url_rule('/patient/appointments-update/<int:appointment_id>',
                     view_func=AppointmentView.as_view('appointments-update'), methods=['GET'])
patient.add_url_rule('/appointments', view_func=AppointmentView.as_view('appointments'), methods=['POST'])
patient.add_url_rule('/appointments-delete',
                     view_func=AppointmentView.as_view('appointments-delete'), methods=['DELETE'])

patient.add_url_rule('/doctors_list_id',
                     view_func=DoctorListDiseasesID.as_view('doctors_list_id'), methods=['GET', 'POST'])

# @login_required
# @patient.route('/patient/profile/<int:patient_id>', methods=['GET', 'POST'])
# def patient_profile(patient_id):
#     patient_data = get_patient_data_id(patient_id)
#     if request.form:
#         data = json.loads(request.form['user_data'])
#
#         address_1 = request.form['address_1']
#         address_2 = request.form['address_2']
#         city = request.form['city']
#         state = request.form['state']
#         pincode = request.form['pincode']
#         country = request.form['country']
#
#         address_exist = Address.query.filter(Address.address_1 == address_1,
#                                              Address.address_2 == address_2,
#                                              Address.city == city).first()
#         if address_exist:
#             address_id = address_exist.id
#             address = Address.query.get_or_404(address_id)
#             address.address_1 = address_1
#             address.address_2 = address_2
#             address.city = city
#             address.state = state
#             address.pincode = pincode
#             address.country = country
#         else:
#             address = Address(address_1=address_1, address_2=address_2, city=city, state=state, pincode=pincode,
#                               country=country)
#             db.session.add(address)
#             db.session.flush()
#             address_id = address.id
#
#         username_exist = User.query.filter(User.username == data['username']).filter(User.id != patient_id).first()
#         if username_exist:
#             flash(f'Username already exists !', 'danger')
#         else:
#             user_data = User.query.get_or_404(patient_id)
#             user_data.name = data['name']
#             user_data.username = data['username']
#             user_data.phone = data['phone']
#             user_data.email = data['email']
#
#             if not address_exist:
#                 user_address_data = User_Address.query.get_or_404(user_id=patient_id)
#                 if user_address_data:
#                     user_address_data.user_id = patient_id
#                     user_address_data.address_id = address_id
#                 else:
#                     user_address = User_Address(user_id=patient_id, address_id=address_id)
#                     db.session.add(user_address)
#
#             db.session.commit()
#             flash(f'Profile Updated Successfully !', 'success')
#             return redirect(url_for('patient.patient_profile', patient_id=patient_id))
#     return render_template('register.html', title='Profile', patient_data=patient_data)

# @login_required
# @patient.route('/doctors_list_id/', methods=['GET', 'POST'])
# def doctors_list_id():
#     doctors_data = DoctorDisease.query \
#         .join(User, DoctorDisease.user_id == User.id) \
#         .filter(DoctorDisease.diseases_id == request.json['data']) \
#         .add_columns(User.id, User.name) \
#         .all()
#     doctor_dict = []
#     for index in range(len(doctors_data)):
#         doctor_dict.append({'id': doctors_data[index].id, 'name': doctors_data[index].name})
#
#     context = {
#         'success': True if doctors_data else False,
#         'doctors_data': doctor_dict
#     }
#     return context

# @login_required
# @patient.route('/patient/appointments/', methods=['GET'])
# def appointments_list():
#     appointments_data = Appointments.query \
#         .join(PatientAppointments, Appointments.id == PatientAppointments.appointment_id) \
#         .join(User, Appointments.user_id == User.id) \
#         .join(Diseases, Appointments.diseases_id == Diseases.id) \
#         .filter(PatientAppointments.user_id == current_user.id) \
#         .add_columns(User.name, Diseases.name, Appointments.apt_datetime, Appointments.is_approved, Appointments.id) \
#         .all()
#     return render_template('appointment_list.html', title='Appointment List', appointments_data=appointments_data,
#                            hide_add_btn=True)


# @login_required
# @patient.route('/patient/appointments-add/', methods=['GET', 'POST'])
# def appointments_add():
#     all_diseases = get_all_diseases()
#     if request.form:
#         custom_datetime = datetime.strptime(request.form['datetime'], '%Y-%m-%dT%H:%M')
#         appointment_data = Appointments(user_id=request.form['doctors'], diseases_id=request.form['diseases'],
#                                         apt_datetime=custom_datetime, description=request.form['description'])
#         db.session.add(appointment_data)
#         db.session.flush()
#         appointment_id = appointment_data.id
#
#         patient_appointment = PatientAppointments(user_id=current_user.id, appointment_id=appointment_id)
#         db.session.add(patient_appointment)
#         db.session.commit()
#         flash(f'Appointment Added Successfully !', 'success')
#         return redirect(url_for('patient.appointments_list'))
#     return render_template('add_appointment.html', title='Add Appointment', diseases=all_diseases, appointment_data=None)


# @login_required
# @patient.route('/patient/appointments-update/<int:appointment_id>', methods=['GET', 'POST'])
# def appointments_update(appointment_id):
#     all_diseases = get_all_diseases()
#     appointment_data = get_appointment_data_id(appointment_id)
#     doctor_data = get_doctor_diseases_id(appointment_data.Appointments.diseases_id)
#
#     if request.form:
#         custom_datetime = datetime.strptime(request.form['datetime'], '%Y-%m-%dT%H:%M')
#         appointment_details = Appointments.query.get_or_404(appointment_id)
#         appointment_details.diseases_id = request.form['diseases']
#         appointment_details.user_id = request.form['doctors']
#         appointment_details.apt_datetime = custom_datetime
#         appointment_details.description = request.form['description']
#         db.session.commit()
#         flash(f'Appointment Updated Successfully !', 'success')
#         return redirect(url_for('patient.appointments_list'))
#
#     return render_template('add_appointment.html', title='Update Appointment', diseases=all_diseases,
#                            appointment_data=appointment_data, doctor_data=doctor_data)


# @login_required
# @patient.route('/patient/appointments-delete/<int:appointment_id>', methods=['GET', 'POST'])
# def appointments_delete(appointment_id):
#     appointment = Appointments.query.get_or_404(appointment_id)
#     db.session.delete(appointment)
#     db.session.commit()
#     flash(f'Appointment Deleted Successfully', 'success')
#     return redirect(url_for('patient.appointments_list'))
