from itsdangerous import TimedJSONWebSignatureSerializer as Serializer
from sqlalchemy import create_engine
from sqlalchemy.orm import declarative_base
from appointment_system import db, login_manager, Config
from flask import current_app
from flask_login import UserMixin
import enum

Base = declarative_base()


#
def db_connect():
    """
    Performs database connection using database settings from settings.py.
    Returns sqlalchemy engine instance
    """
    return create_engine(Config.SQLALCHEMY_DATABASE_URI, echo=True)


def create_table(engine):
    Base.metadata.create_all(engine)


@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))


class UserType(enum.Enum):
    hospital_admin = 'hospital_admin'
    doctor = 'doctor'
    patient = 'patient'


class User(db.Model, UserMixin):
    __tablename__ = "user"

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(120), unique=False, nullable=False)
    username = db.Column(db.String(20), unique=True, nullable=False)
    phone = db.Column(db.String(11), unique=False, nullable=False)
    email = db.Column(db.String(120), unique=True, nullable=False)
    password = db.Column(db.String(60), nullable=False)
    user_type = db.Column(db.Enum(UserType), default=UserType.hospital_admin, nullable=False)
    # M-to-M for user and address
    addresses = db.relationship('Address', secondary='user_address', backref=db.backref('users'), lazy='joined')

    def get_reset_token(self, expires_seconds=1800):
        s = Serializer(current_app.config['SECRET_KEY'], expires_seconds)
        return s.dumps({'user_id': self.id}).decode('utf-8')

    @staticmethod
    def verify_reset_token(token):
        s = Serializer(current_app.config['SECRET_KEY'])
        try:
            user_id = s.loads(token)['user_id']
        except:
            return None
        return User.query.get(user_id)

    def __repr__(self):
        return f"User ('{self.username}','{self.email}')"


class Address(db.Model):
    __tablename__ = "address"

    id = db.Column(db.Integer, primary_key=True)
    address_1 = db.Column(db.String(120), unique=False, nullable=False)
    address_2 = db.Column(db.String(120), unique=False, nullable=False)
    city = db.Column(db.String(50), unique=False, nullable=False)
    state = db.Column(db.String(50), unique=False, nullable=False)
    pincode = db.Column(db.String(10), unique=False, nullable=False)
    country = db.Column(db.String(50), unique=False, nullable=False)

    def __repr__(self):
        return f"Address ('{self.address_1}','{self.address_2}')"


class User_Address(db.Model):
    __tablename__ = "user_address"

    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    address_id = db.Column(db.Integer, db.ForeignKey('address.id'), nullable=False)

    def __repr__(self):
        return f"User Address ('{self.user_id}','{self.address_id}')"


class Diseases(db.Model):
    __tablename__ = "diseases"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(120), unique=True, nullable=False)

    def __repr__(self):
        return f"Disease ('{self.id}','{self.name}')"


class DoctorDisease(db.Model):
    __tablename__ = "doctor_disease"
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    diseases_id = db.Column(db.Integer, db.ForeignKey('diseases.id'), nullable=False)

    def __repr__(self):
        return f"Doctor Diseases ('{self.user_id}','{self.diseases_id}')"


class Appointments(db.Model):
    __tablename__ = "appointments"
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    diseases_id = db.Column(db.Integer, db.ForeignKey('diseases.id'), nullable=False)
    apt_datetime = db.Column(db.DATETIME, nullable=False)
    is_approved = db.Column(db.BOOLEAN, default=False)
    description = db.Column(db.TEXT)

    def __repr__(self):
        return f"Appointments ('{self.user_id}','{self.diseases_id}')"


class PatientAppointments(db.Model):
    __tablename__ = "patient_appointments"
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    appointment_id = db.Column(db.Integer, db.ForeignKey('appointments.id'), nullable=False)

    def __repr__(self):
        return f"Patient Appointments ('{self.user_id}','{self.appointment_id}')"
